import threading
import urllib.request
import urllib.parse
import os
import requests
import json

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.common import exceptions
from bs4 import BeautifulSoup

import time
from selenium.webdriver.common.keys import Keys

from private_data.CONFIG import CONFIG

GALLERY_URL = "http://ai2.appinventor.mit.edu/?galleryId=6492303274934272"
URL_BASE = "http://ai2.appinventor.mit.edu"



# Define Browser Options
chrome_options = Options()
download_setting = {"download.default_directory" : "/Users/adarsl/Downloads/appinventor_projects"}
chrome_options.add_experimental_option("prefs", download_setting)
# chrome_options.add_argument("--headless") # Hides the browser window

mail_address = CONFIG["mail_address"]
password = CONFIG["password"]
chrome_path = CONFIG["chrome_path"]

def trash_all_projects(driver):
    driver.find_element_by_xpath("//div[@class='html-face' and text()='My Projects ▾ ']").click()
    time.sleep(0.2)
    driver.find_element_by_id("gwt-uid-1").click()

    page_html = driver.page_source
    soup = BeautifulSoup(page_html, 'html.parser')
    projects_table = soup.find("table", attrs={"class": "ode-ProjectTable"})

    checkboxes = projects_table.findAll("input")
    for tag in checkboxes:

        driver.find_element_by_xpath("//input[@type='checkbox' and @name='" + tag.attrs['name'] + "']").click()

def delete_all_projects(driver):
    driver.find_element_by_xpath("//div[@class='html-face' and text()='My Projects ▾ ']").click()
    time.sleep(1)
    driver.find_element_by_id("gwt-uid-1").click()
    time.sleep(1)

    driver.find_element_by_xpath("/html/body/table/tbody/tr[2]/td/table/tbody/tr/td[2]/div/div[1]/table/tbody/tr[1]/td/table/tbody/tr/td[1]/table/tbody/tr/td[4]/div/div").click()

    page_html = driver.page_source
    soup = BeautifulSoup(page_html, 'html.parser')
    projects_table = soup.find("table", attrs={"class": "ode-ProjectTable"})

    checkboxes = projects_table.findAll("input")
    for tag in checkboxes:
        driver.find_element_by_xpath("//input[@type='checkbox' and @name='" + tag.attrs['name'] + "']").click()


def select_project(project_id, project_name):
    print("now selecting " + project_name)

    random_hash = 'ShQs5$gAA'

    # should be changed every few hours for now
    app_inventor_key = 'ANY9Ve1Kl9lew_NyJ7oEhVYo6IUbyMXYzQBrlSWLjfa1wbOTiG2OGDCeTzbQd51DiINFU4j5EanMp8WNdL05eYUU6abtkaKwjqFZATKnVhaRAchnVi2JblUlMhcyLi_FBxm5lJobzhx8'

    try:
        response = requests.post(f"{URL_BASE}/ode/projects",
                                 headers={"accept": "*/*",
                                          'Cookie': f"_ga=GA1.2.1250893631.1584010301; _gid=GA1.2.1352219692.1586074987; __utma=102331409.434455763.1586108848.1586108848.1586108848.1; __utmc=102331409; __utmz=102331409.1586108848.1.1.utmcsr=appinventor.mit.edu|utmccn=(referral)|utmcmd=referral|utmcct=/; AppInventor={app_inventor_key}; __utmt=1; __utmb=102331409.2.10.1586108848",
                                          "accept-language": "en-US,en;q=0.9,he-IL;q=0.8,he;q=0.7",
                                          "content-type": "text/x-gwt-rpc; charset=UTF-8",
                                          "x-gwt-module-base": "http://ai2.appinventor.mit.edu/ode/",
                                          "x-gwt-permutation": "DD476B8B3D4F8E4F544AC066608CFD64",
                                          "Referer": "http://ai2.appinventor.mit.edu/?galleryId=6492303274934272",
                                          "referrerPolicy": "no-referrer-when-downgrade"
                                          },
                                 data=f"7|0|8|http://ai2.appinventor.mit.edu/ode/|2E188966114025184B8BAC9CC960B5FF|com.google.appinventor.shared.rpc.project.ProjectService|newProjectFromGallery|java.lang.String/2004016611|J|{project_name}|gallery/apps/{project_id}/aia|1|2|3|4|3|5|5|6|7|8|{random_hash}|",
                                 )

        return response.content.decode()
    except UnicodeEncodeError as e:
        print("project name: " + project_name + " is not correctly encoded. will skip this project for now")
        return "failure"



def get_project_ids():
    response = requests.post(f"{URL_BASE}/ode/gallery",
                             headers={"accept": "*/*",
                                      'Cookie': "_ga=GA1.2.79497099.1585762438; _gid=GA1.2.1237010174.1585762438; __utma=102331409.79497099.1585762438.1585762740.1585762740.1; __utmc=102331409; __utmz=102331409.1585762740.1.1.utmcsr=appinventor.mit.edu|utmccn=(referral)|utmcmd=referral|utmcct=/; AppInventor=ANY9Ve3S96P5IoX1FNLON8obcvfdABjtK3PbPDQELVz8am0VYES7uYl_F6vIUONAl_gDgN5b-osv7Jow7ivjeiL8taZ_y41IVL1y8wLx7Uk0dHEUOB7iLFvYORIr0x4G3IlbA6s-1vDa; __utmt=1; __utmb=102331409.7.10.1585762740",
                                      "accept-language": "en-US,en;q=0.9,he-IL;q=0.8,he;q=0.7",
                                      "content-type": "text/x-gwt-rpc; charset=UTF-8",
                                      "x-gwt-module-base": "http://ai2.appinventor.mit.edu/ode/",
                                      "x-gwt-permutation": "DD476B8B3D4F8E4F544AC066608CFD64",
                                      "referrer": "http://ai2.appinventor.mit.edu/",
                                      "referrerPolicy": "no-referrer-when-downgrade",
                                      },
                             data="7|0|5|http://ai2.appinventor.mit.edu/ode/|4D5E2274C0126AB1AA72FD2DC20145ED|com.google.appinventor.shared.rpc.project.GalleryService|getRecentApps|I|1|2|3|4|2|5|5|0|10|",
                             )

    return response.content.decode()


def main():
    print(os.getcwd())
    driver = webdriver.Chrome(executable_path=chrome_path, chrome_options=chrome_options)
    users_projects = start(driver)

def start(driver):
    try:
        with open('../private_data/users_projects.json', 'r') as fp:
            users_projects = json.load(fp)
    except IOError as e:
        users_projects = {}

    # loading first page, which is google login
    driver.get(GALLERY_URL)
    time.sleep(5)
    authenticate_google_login(driver)
    # checking if welcome window popped up
    resolve_welcome_popup(driver)

    # moving to gallery page
    move_to_gallery(driver)
    for i in range(3):
        # getting more projects to show up on page
        show_more_projects(driver)
        time.sleep(5)
    # get the gallery cards
    gallery_cards_divs = get_gallery_cards_divs(driver)
    # iterate over acquired gallery cards
    for gallery_cards_div in gallery_cards_divs:
        # enter the project's page
        try:
            enter_project_page(driver, gallery_cards_div)
        except (exceptions.NoSuchElementException, exceptions.ElementNotInteractableException) as e:
            print("moving to next gallery card div")
            continue

        project_link = "http://" + driver.find_element_by_xpath(
            "//input[@type='text' and @class='gwt-TextBox action-textbox']").get_attribute('value')
        curr_project_id = urllib.parse.parse_qs(urllib.parse.urlsplit(project_link).query)["galleryId"][0]
        curr_project_name = str(gallery_cards_div.contents[1].contents[0].contents[0].contents[0])
        curr_user_name = str(gallery_cards_div.contents[1].contents[1].contents[0].contents[0])

        try:
            if [curr_project_name, curr_project_id] in users_projects[curr_user_name]:
                # project already downloaded
                move_to_gallery()
                continue
        except Exception as e: # its a new project
            pass

        # select project on client side for later download
        response = select_project(curr_project_id, curr_project_name)
        if "//OK" in response:
            print(curr_project_name + " selected succesfully")
            if curr_user_name not in users_projects.keys():
                users_projects[curr_user_name] = [[curr_project_name, curr_project_id]]
            else:
                users_projects[curr_user_name].append([curr_project_name, curr_project_id])
        else:
            print("project name: " + curr_project_name + " could not be selected at the time")
        # moving back to gallery page
        move_to_gallery(driver)

    with open('../private_data/users_projects.json', 'w') as fp:
        json.dump(users_projects, fp, sort_keys=True, indent=4)
    download_all_projects(driver)
    time.sleep(120)

    driver.quit()


# download all the opened projects
def download_all_projects(driver):
    print("now downloading all selected projects")

    #first need to refresh to page to load the projects
    driver.refresh()
    time.sleep(10)
    driver.find_element_by_xpath("//div[@class='html-face' and text()='My Projects ▾ ']").click()
    time.sleep(0.2)
    driver.find_element_by_id("gwt-uid-10").click()
    time.sleep(1)
    driver.switch_to.alert.accept()


# download the given project
def download_project(driver, project_link):
    project_id = urllib.parse.parse_qs(urllib.parse.urlsplit(project_link).query)["galleryId"][0]
    download_link = "http://ai2.appinventor.mit.edu/ode/download/project-source/" + project_id

    #downloading the aia
    urllib.request.urlretrieve(download_link, project_id + ".aia")



# target for thread. to start the given project in a new tab and then open it on client.
# def open_project(driver, project_link):
#     # move project to new window
#     driver.execute_script("window.open('');")
#     window_handlers = driver.window_handles
#     driver.switch_to.window(driver.window_handles[-1])
#     driver.get(project_link)
#     time.sleep(5)
#
#     # open project for client
#     driver.find_element_by_xpath("/html/body/table/tbody/tr[2]/td/table/tbody/tr/td[2]/div/div[4]/table/tbody/tr[2]/td/table/tbody/tr/td/div/div/table/tbody/tr/td/div/div/div/div[1]/div[1]/div/button").click()
#     time.sleep(0.5)
#     driver.find_element_by_xpath("//button[@type='button' and text()='OK']").click()
#
# # start up a new thread to open the given project on client
# def start_opener(driver, project_link):
#     opener = threading.Thread(target=open_project, args=(driver, project_link))
#     opener.start()


# while on gallery, given a gallery card div this method open the page-tab for the given gallery card.
def enter_project_page(driver, gallery_cards_div):
    project_title = gallery_cards_div.find("div", class_="gallery-title").contents[0]
    print("trying to enter " + project_title + " project page")

    try:
        # entering project tab in  gallery page
        driver.find_element_by_xpath("//div[@class='gallery-title' and text()='" + project_title + "']").click()
        print("succesfully entered " + project_title + " project page")
        time.sleep(5)
    except (exceptions.NoSuchElementException, exceptions.ElementNotInteractableException) as e:
        print("could not find the project page for: " + project_title)
        raise e



# while on the gallery page, this method click the 'show more apps' button to load more apps to the page.
def show_more_projects(driver):
    print("clicking on the 'show more' button")
    try:
        driver.find_element_by_xpath("//div[@class='gwt-Label active' and text()='More Apps']").click()
    except exceptions.NoSuchElementException as e:
        print("Couldnt show more projects: " + e.msg)
        time.sleep(3)
        driver.find_element_by_xpath("//div[@class='gwt-Label active' and text()='More Apps']").click()

    time.sleep(5)

# while on gallery page, returns all shown (and some not shown) division of gallery cards, which represent different objects
def get_gallery_cards_divs(driver):
    print("getting all the gallery cards division shown right now at the gallery")
    page_html = driver.page_source
    soup = BeautifulSoup(page_html, 'html.parser')
    gallery_cards_divs = soup.findAll("div", class_="gallery-card")
    return gallery_cards_divs

# while on any page in appinventor, moves the driver back to the gallery page.
def move_to_gallery(driver):
    print("moving back to gallery main page")
    driver.find_element_by_xpath("//div[@class='html-face' and text()='Gallery']").click()
    time.sleep(7)

# check if a welcome popup has popped up, and if so then get rid of it while checking 'do not show again'
def resolve_welcome_popup(driver):
    if driver.find_elements_by_xpath("//div[@class='Caption' and text()='Welcome to App Inventor!']"):
        # click the 'do not show again' button and continue
        driver.find_element_by_id("gwt-uid-58").click()
        time.sleep(1)
        driver.find_element_by_xpath("//button[@type='button' and text()='Continue']").click()
        time.sleep(1)


# can only be used when on google authenticate page.
# use username and password to go through
def authenticate_google_login(driver):
    print("trying to authenticate google login")
    # entering email and then password, moving to appinventor page

    driver.find_element_by_id("identifierId").send_keys(mail_address)
    driver.find_element_by_id("identifierNext").click()
    time.sleep(3)

    driver.find_element_by_name("password").send_keys(password)
    driver.find_element_by_id("passwordNext").click()
    print("successful google login ")
    time.sleep(10)


if __name__ == "__main__":
    main()